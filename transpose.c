#include <stdio.h>
int main()
{
    int a[10][10], transpose[10][10], m, n, i, j;
    printf("Enter number of rows and columns in the matrix: ");
    scanf("%d %d", &m, &n);
  
    printf("\nEnter elements of matrix:\n");
    for(i=0; i<m; ++i)
        for(j=0; j<n; ++j)
        {

            scanf("%d", &a[i][j]);
        }
  
    printf("\nEntered Matrix: \n");
    for(i=0; i<m; ++i)
        for(j=0; j<n; ++j)
        {
            printf("%d  ", a[i][j]);
            if (j == m-1)
                printf("\n\n");
        }
  
    for(i=0; i<m; ++i)
        for(j=0; j<n; ++j)
        {
            transpose[j][i] = a[i][j];
        }
   
    printf("\nTranspose of Matrix:\n");
    for(i=0; i<n; ++i)
        for(j=0; j<m; ++j)
        {
            printf("%d  ",transpose[i][j]);
            if(j==m-1)
                printf("\n\n");
        }
    return 0;
}